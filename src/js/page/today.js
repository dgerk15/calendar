/**
 * Created by Eugene on 21-10-2016.
 */


var userEvent = [];
$(document).ready(function () {
    $("#todayTab").click(function () {
        selectAll(table.events, function (data) {
            var k = 0;
            for(var i = 0; i < data.length; i++){
                if (data[i].userId == localStorage.id){
                    userEvent[k] = data[i];
                    k++;
                }
            }
            viewTableEvents();
        })
        viewTable();

    });
});

    function viewTable() {
        var numWeek;
        var year = new Date().getFullYear();
        var month = new Date().getMonth();
        var today = new Date(year, month, 0).getTime();
        var now = new Date().getTime();
        var week = Math.round((now - today) / (1000 * 60 * 60 * 24 * 7));
        if (week % 2) {
           numWeek = 1
        } else {
            numWeek = 0;
        }

        var day = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
        var dayRus = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"];
        var myWeek = ["first", "second"];
        var myTtoday = (new Date).getDay();
        $("#dayOfWeek").text("Расписание " + dayRus[myTtoday]);

        // switch (today){
        //     case 0:
        //         day = "sunday";
        //         break;
        //     case 1:
        //         day = "monday";
        //         break;
        //     case 2:
        //         day = "tuesday";
        //         break;
        //     case 3:
        //         day = "wednesday";
        //         break;
        //     case 4:
        //         day = "thursday";
        //         break;
        //     case 5:
        //         day = "friday";
        //         break;
        //     case 6:
        //         day = "saturday";
        //         break;
        // }
        
            $("#sheduleTable").children("tbody").html(function () {
                var strtable = "";
                for(var i = 0; i < 7; i++) {
                    if (sheduleData.payload.schedule[myWeek[numWeek]][day[myTtoday]].data[i].event) {
                        strtable += "<tr>";

                        strtable += "<td class = 'count'>" + (i + 1) + "</td>";
                        strtable += "<td class = 'time'>" + sheduleData.payload.schedule[myWeek[numWeek]][day[myTtoday]].data[i].time + "</td>";
                        strtable += "<td class = 'class'>" + sheduleData.payload.schedule[myWeek[numWeek]][day[myTtoday]].data[i].event + "</td>";

                        strtable += "</tr>";
                    }
                }
                return strtable;
            });
            // bindDeleteClick( $('#tblUsers .userDelete') );
    };

    function viewTableEvents() {
        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
        var todayFullDate = date + "." + month + "." + year;
        $("#dayOfWeek1").text("События " + todayFullDate);
        $("#eventTable").children("tbody").html(function () {
            var strtable = "";
            for (var i = 0; i < userEvent.length; i++){
                console.log(todayFullDate + ' = ' + userEvent[i].date);
                if(userEvent[i].date == todayFullDate){
                strtable += "<tr>";

                strtable += "<td class = 'count'>" + (i + 1) + "</td>";
                strtable += "<td class = 'title'>" + userEvent[i].title + "</td>";
                strtable += "<td class = 'date'>" + userEvent[i].date + "</td>";
                strtable += "<td class = 'time'>" + userEvent[i].time + "</td>";
                strtable += "<td class = 'description'>" + userEvent[i].description + "</td>";

                strtable += "</tr>";
                }
            }
            return strtable;
        });
    };

function fillEvent(){
    $("#eventNameDlg").val("");
    $("#eventDataDlg").val("");
    $("#eventTimeStart").val("");
    $("#eventTimeEnd").val("");
    $("#eventAboutDlg").val("");
};