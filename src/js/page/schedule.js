$(document).ready(function () {
    // var thisWeek = "0";

    $("#scheduleTab").click(function () {
        viewTable2();
    });
});

function viewTable2() {

    var numWeek;
    var year = new Date().getFullYear();
    var month = new Date().getMonth();
    var today = new Date(year, month, 0).getTime();
    var now = new Date().getTime();
    var week = Math.round((now - today) / (1000 * 60 * 60 * 24 * 7));
    if (week % 2) {
        numWeek = 1
    } else {
        numWeek = 0;
    }

    var weekName;
    switch (numWeek){
        case 0:
            weekName = "Нечётная неделя";
            break;
        case 1:
            weekName = "Чётная неделя";
            break;
    }
    $("#hWeek").text(weekName);


    var day = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
    var myWeek = ["first", "second"];

    $("#tblRaspis").children("tbody").html(function () {
        var strtable = "";
        for(var i = 1; i < 7; i++) {
            strtable += "<tr>";
            strtable += "<td class = 'th_row text-center th_row_day2'>" + sheduleData.payload.schedule[myWeek[numWeek]][day[i]].day + "</td>";
            for (var j = 0; j < 7; j++ ) {
                    if(sheduleData.payload.schedule[myWeek[numWeek]][day[i]].data[j].event){
                            strtable += "<td class = 'row_rowspan text-center'>" + sheduleData.payload.schedule[myWeek[numWeek]][day[i]].data[j].event + "</td>";
                    }else{
                        strtable += "<td class = 'row_rowspan'>" + " " + "</td>";
                    }
            }
            strtable += "</tr>";
        }
        return strtable;
    });
};