/**
 * Created by Eugene on 21-10-2016.
 */
$(document).ready(function () {

    $("#userProfile").click(function () {
        $("#userProfileDlg").modal({
            backdrop : true
        });
        // alert(userData.payload.group);
        // console.log((new Date()).getDay());
    });
});


function fillUserProfile(data){
    $("#userProfileHeader").text(data.firstName + ' ' + data.lastName);
    $("#userNameDlg").val(data.firstName);
    $("#userSurNameDlg").val(data.lastName);
    $("#userNameLoginDlg").val(data.userName);
    $("#userGroupDlg").val(data.group);
    $("#userEmailDlg").val(data.email);
}