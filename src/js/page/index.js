/**
 * Created by Eugene on 21-10-2016.
 */
var userProfile;

$(document).ready(function(){
    // localStorage.clear();
    
    verification();

    // $("#attempts").append(localStorage.attempts);
    // $("#points").append(localStorage.points);

    $("#btnBack").click(function () {
        $("#thisbtn").removeAttr('hidden');
    });

    $("#registration").click(function () {
        $("#reg").load("registration.html");
        $("#thisbtn").attr('hidden',true);
    });

    $("#login").click(function () {
        $("#reg").load("login.html");
        $("#thisbtn").attr('hidden',true);
    });

    $(function(){
        $("#calendarDiv").load("calendar.html");
    });

    $(function(){
        $("#today").load("today.html");
    });

    $(function(){
        $("#schedule").load("schedule.html");
    });

    $("#btnUserLogout").click(function () {
        // alert("exit");
        localStorage.clear();
        location.reload();
    });

});

function verification() {
    if (localStorage.id != undefined) {
        $("#thisbtn").attr("hidden", true);
        $("#reg").attr("hidden", true);
        $("#calendarForm").attr("hidden", false);
        selectOne(table.users, localStorage.id, function (data) {
            userProfile = data;
            $("#lnkUserProfile").removeAttr('hidden');
            $("#logoutText").attr("hidden", false);
            $("#userProfile").text(userProfile.firstName + ' ' + userProfile.lastName);
            fillUserProfile(userProfile);

            $("#tab-1").ready(function () {
                setTimeout(getShedule(), 1000); //Задержка в 1 сек перед выгрузкой расписания в "сегодня"
            });
        });
    } else{
        $("#thisbtn").attr("hidden", false);
        $("#logoutText").attr("hidden", true);
    }
}