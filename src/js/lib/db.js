/**
 * Created by Eugene on 25-10-2016.
 */

var baseURL = 'https://api.mlab.com/api/1';
var apiKey = 'BL4Lu_qe4n1CIll2Uj3k1hUaRrLUCVoO';
var database = 'radik_calendar';

var table = {
    users: 'users',
    events: 'events',
};

/*******************************************************************************************************************
 *
 * MODELS
 *
 ******************************************************************************************************************/

/**
 * 
 * @param firstName
 * @param lastName
 * @param group
 * @param userName
 * @param email
 * @param password
 * @constructor
 */
function UserModel(firstName, lastName, group, userName, email, password) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.group = group;
    this.userName = userName;
    this.email = email;
    this.password = password;
}

/**
 * 
 * @param userId
 * @param title
 * @param date
 * @param time
 * @param description
 * @constructor
 */
function EventModel(userId, title, date, time, description) {
    this.userId = userId;
    this.title = title;
    this.date = date;
    this.time = time;
    this.description = description;
}



/*******************************************************************************************************************
 *
 * DATA BASE FUNCTIONS
 *
 ******************************************************************************************************************/

function insert(table, model, successFunction) {
    $.ajax( { url: baseURL + "/databases/" + database + "/collections/" + table + "?apiKey=" + apiKey,
        data: JSON.stringify( model ),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        success: function(data){
            successFunction(data);
        },
        error: function(){
        }
    });
}

function update(table, id, parametrs, successFunction) {
    $.ajax( { url: baseURL + "/databases/" + database + "/collections/" + table + "/" + id + "?apiKey=" + apiKey,
        data: JSON.stringify( { "$set" : parametrs } ),
        type: "PUT",
        contentType: "application/json;charset=utf-8",
        success: function(data){
            successFunction(data);
        },
        error: function(){
        }
    });
}

function remove(table, id, successFunction) {
    $.ajax( { url: baseURL + "/databases/" + database + "/collections/" + table + "/" + id + "?apiKey=" + apiKey,
        type: "DELETE",
        async: true,
        timeout: 300000,
        success: function(data){
            successFunction(data);
        },
        error: function(xhr, status, err){
        }
    });
}

function removeM(table, parametrs, successFunction) {
    $.ajax( { url: baseURL + "/databases/" + database + "/collections/" + table + "?q=" + parametrs + "&apiKey=" + apiKey,
        data: JSON.stringify([]),
        type: "PUT",
        contentType: "application/json;charset=utf-8",
        success: function(data){
            successFunction(data);
        },
        error: function(xhr, status, err){
        }
    });
}

function selectAll(table, successFunction) {
    var sort = "s={'date': 1}&";
    $.ajax({
        url: baseURL + "/databases/" + database + "/collections/" + table + "?" + sort + "apiKey=" + apiKey,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            successFunction(data);
        },
        error: function () {
        }
    });
}

function selectOne(table, id, successFunction) {
    $.ajax({
        url: baseURL + "/databases/" + database + "/collections/" + table + "/" + id + "?apiKey=" + apiKey,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            successFunction(data);
        },
        error: function () {
        }
    });
}

function selectOneByLoginAndPass(email, successFunction) {
    $.ajax({
        url: baseURL + "/databases/" + database + "/collections/" + table.users + "/" + email + "?apiKey=" + apiKey,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            successFunction(data);
        },
        error: function (xhr, status, err) {
        }
    });
}


// example

//    insert(table.tasks, taskModel, function (data) {
//        console.log(data);
//    });
//
//    selectAll(table.tasks, function (data) {
//        console.log(data);
//    });

//    var taskModel = new TaskModel(
//            'How learn js?',
//            'sdsfsdfdgsdgsdgdg'
//    );

//    var ideaModel = new IdeaModel(
//            'id',
//            'sdsfsdfdgsdgsdgdg'
//    );

//removeM(table.ideas, "{'task_id': '573776cdf8c2e77dbb4c1fdc'}", function (data) {
//    console.log(data);
//});